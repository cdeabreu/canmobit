from django.db import models
from django.utils.translation import ugettext_lazy as _


class Ordenable(models.Model):
    order = models.PositiveIntegerField(_('Orden'), default=0)

    class Meta:
        abstract = True


class Timestampable(models.Model):
    creation_date = models.DateTimeField(_('Fecha de creación'), auto_now=True)

    class Meta:
        abstract = True


class Localizable(models.Model):
    neighborhood = models.CharField(_('Barrio'), max_length=80)
    location = models.CharField(_('Localidad'), max_length=140)
    state = models.CharField(_('Departamento'), max_length=80)

    class Meta:
        abstract = True


class Descriptable(models.Model):
    short_description = models.TextField(_('Descripcíon breve'), max_length=80, null=True, blank=True)
    full_description = models.TextField(_('Descripcíon'), max_length=200, null=True, blank=True)

    class Meta:
        abstract = True


