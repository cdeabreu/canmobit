from django.urls import path, re_path

from . import views

urlpatterns = [
    path('', views.list_pet_posts, name='list_pet_posts'),
    path('pet/add/', views.add_pet_post, name='add_pet_post'),
    re_path(r'pet/(?P<pet_id>\d+)/$', views.pet_post_detail, name='pet_post_detail'),
    re_path(r'pet/(?P<pet_id>\d+)/edit/$', views.edit_pet_post, name='edit_pet_post'),
]
