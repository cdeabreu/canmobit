from django.shortcuts import render
from django.http import HttpResponse

from . import filters

def filter_and_render(func):
    def _inner(*args, **kwargs):
        request = args[0]
        request.active_filters = []
        objs, template, extra_ctx = func(*args, **kwargs)
        objs = filters.filter_by_status(objs, request)
        objs = filters.filter_by_type(objs, request)
        objs = filters.filter_by_datetime(objs, request)
        objs = filters.filter_by_age_estimation(objs, request)
        objs = filters.filter_by_size(objs, request)
        objs = filters.filter_by_sex(objs, request)
        ctx = {
            'objs': objs,
        }
        if extra_ctx:
            ctx.update(extra_ctx)

        return render(request, template, ctx)
    return _inner

