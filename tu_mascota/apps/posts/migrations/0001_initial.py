# Generated by Django 2.1.7 on 2019-04-06 03:29

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Donation',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('creation_date', models.DateTimeField(auto_now=True, verbose_name='Fecha de creación')),
                ('neighborhood', models.CharField(max_length=80, verbose_name='Barrio')),
                ('location', models.CharField(max_length=140, verbose_name='Localidad')),
                ('state', models.CharField(max_length=80, verbose_name='Departamento')),
                ('short_description', models.TextField(blank=True, max_length=80, null=True, verbose_name='Descripcíon breve')),
                ('full_description', models.TextField(blank=True, max_length=200, null=True, verbose_name='Descripcíon')),
                ('status', models.CharField(choices=[('donated', 'Donado'), ('available', 'Disponible')], max_length=20, verbose_name='Estado')),
                ('created_by', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='donation_publications', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='DonationImage',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('image', models.ImageField(upload_to='posts/images/donations/', verbose_name='Imagen')),
                ('post', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='donation_images', to='posts.Donation')),
            ],
        ),
        migrations.CreateModel(
            name='Pet',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('creation_date', models.DateTimeField(auto_now=True, verbose_name='Fecha de creación')),
                ('neighborhood', models.CharField(max_length=80, verbose_name='Barrio')),
                ('location', models.CharField(max_length=140, verbose_name='Localidad')),
                ('state', models.CharField(max_length=80, verbose_name='Departamento')),
                ('short_description', models.TextField(blank=True, max_length=80, null=True, verbose_name='Descripcíon breve')),
                ('full_description', models.TextField(blank=True, max_length=200, null=True, verbose_name='Descripcíon')),
                ('status', models.CharField(choices=[('missing', 'Perdido'), ('finded', 'Encontrado')], max_length=80, verbose_name='Estado')),
                ('type', models.CharField(choices=[('dog', 'Perro'), ('cat', 'Gato'), ('other', 'Otros'), ('capi', 'Carpincho')], max_length=80, verbose_name='Tipo de mascota')),
                ('age_estimation', models.CharField(choices=[('puppy', 'Cachorro'), ('young', 'Joven'), ('adult', 'Adulto')], max_length=20, verbose_name='Edad aproximada')),
                ('sex', models.CharField(choices=[('female', 'Hembra'), ('male', 'Macho')], max_length=80, verbose_name='Sexo')),
                ('size', models.CharField(choices=[('small', 'Chico'), ('medium', 'Mediano'), ('large', 'Grande')], max_length=20, verbose_name='Tamaño')),
                ('color', models.CharField(max_length=40, verbose_name='Color')),
                ('hair', models.CharField(max_length=40, verbose_name='Pelaje')),
                ('created_by', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='publications', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='PetImage',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('image', models.ImageField(upload_to='posts/images/pets/', verbose_name='Imagen')),
                ('post', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='pet_images', to='posts.Pet')),
            ],
        ),
    ]
