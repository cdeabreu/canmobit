import datetime
from datetime import timedelta

from django.shortcuts import render


DATETIME_FILTER = (
    ('last_0', 'Hoy'),
    ('last_3', 'Últimos 3 días'),
    ('last_7', 'Últimos 7 días'),
    ('last_14', 'Últimos 14 días'),
    ('last_30', 'Último mes'),
    ('last_90', 'Últimos 3 meses'),
)


def filter_by_status(qs, request):
    if 'status' not in  request.GET:
        return qs
    value = request.GET.get('status')
    if value in ['all', '']:
        return qs
    try:
        filtered = qs.filter(status=value)
        request.active_filters.append(value)
        return filtered
    except Exception:
        return qs


def filter_by_sex(qs, request):
    if 'sex' not in  request.GET:
        return qs
    value = request.GET.get('sex')
    if value in ['all', '']:
        return qs
    try:
        filtered = qs.filter(sex=value)
        request.active_filters.append(value)
        return filtered
    except Exception:
        return qs


def filter_by_age_estimation(qs, request):
    if 'age_estimation' not in  request.GET:
        return qs
    value = request.GET.get('age_estimation')
    if value in ['all', '']:
        return qs
    try:
        filtered = qs.filter(age_estimation=value)
        request.active_filters.append(value)
        return filtered
    except Exception:
        return qs


def filter_by_size(qs, request):
    if 'size' not in  request.GET:
        return qs
    value = request.GET.get('size')
    if value in ['all', '']:
        return qs
    try:
        filtered = qs.filter(size=value)
        request.active_filters.append(value)
        return filtered
    except Exception:
        return qs


def filter_by_datetime(qs, request):
    if 'datetime' not in  request.GET:
        return qs
    value = request.GET.get('datetime')
    if value in ['all', '']:
        return qs
    days_delta = int(value.split('_')[-1])
    now_date = datetime.datetime.now()
    now = datetime.datetime.combine(
        now_date, datetime.time(0,0)
    )
    past = now - timedelta(days=days_delta)
    try:
        filtered = qs.filter(creation_date__gte=past)
        request.active_filters.append(value)
        return filtered
    except Exception:
        return qs


def filter_by_type(qs, request):
    if 'type' not in  request.GET:
        return qs
    value = request.GET.get('type')
    if value in ['all', '']:
        return qs
    try:
        filtered = qs.filter(type=value)
        request.active_filters.append(value)
        return filtered
    except Execption:
        return qs


