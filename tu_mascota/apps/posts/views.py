from django.shortcuts import render, redirect, get_object_or_404

from .models import Pet, PetImage
from .models import STATUS, PET, SEX, AGE, SIZE
from . import decorators
from . import forms
from .filters import DATETIME_FILTER
# Create your views here.


@decorators.filter_and_render
def list_pet_posts(request):
    template = 'publications.html'
    pets = Pet.objects.all()
    filters_dicts = [
        ('Estado', 'status', STATUS),
        ('Fecha', 'datetime', DATETIME_FILTER),
        ('Tipo', 'type', PET),
        ('Sexo', 'sex', SEX),
        ('Edad', 'age_estimation', AGE),
        ('Tamaño', 'size', SIZE),
    ]
    ctx = {'filters_dicts': filters_dicts}
    return (pets,template, ctx)


def add_pet_post(request):
    template='add_pet_post.html'
    if request.method == 'POST':
        pet_form = forms.PetForm(request.POST, prefix='pet')
        pet_images_formset = forms.PetImageFormset(
            request.POST, request.FILES, prefix='pet_images'
        )
        if all([pet_form.is_valid(), pet_images_formset.is_valid()]):
            pet_post = pet_form.save(commit=False)
            pet_post.created_by = request.user
            pet_post.save()
            for pet_image_form in pet_images_formset:
                pet_image = pet_image_form.save(commit=False)
                pet_image.post = pet_post
                pet_image.save()

            return redirect('posts:list_pet_posts')
        ctx = {
            'pet_form': pet_form,
            'pet_images_formset': pet_images_formset
        }
        return render(request, template, ctx)

    pet_form =forms.PetForm(prefix='pet')
    pet_images_formset = forms.PetImageFormset(
        queryset=PetImage.objects.none(), prefix='pet_images'
    )
    ctx = {
        'pet_form': pet_form,
        'pet_images_formset': pet_images_formset
    }
    return render(request, template, ctx)


def pet_post_detail(request, pet_id):
    template='pet_post_detail.html'
    pet_post = get_object_or_404(Pet, pk=pet_id)
    return render(request, template, {'pet_post': pet_post})


def edit_pet_post(request, pet_id):
    pet_post = get_object_or_404(Pet, pk=pet_id)
    if request.method == 'POST':
        pet_form = forms.PetForm(request.POST, instance=pet_post, prefix='pet')
        pet_images_formset = forms.PetImageFormset(
            request.POST, request.FILES, prefix='pet_images'
        )

        if all([pet_form.is_valid(), pet_images_formset.is_valid()]):
            pet_post = pet_form.save()
            for pet_image_form in pet_images_formset:
                pet_image = pet_image_form.save(commit=False)
                pet_image.post = pet_post
                pet_image.save()

            return redirect('publications:pet_list')
        ctx = {
            'pet_form': pet_form,
            'pet_images_formset': pet_images_formset
        }
        return render(request, template, ctx)

    pet_form = PetForm(instance=pet_post, prefix='pet')
    pet_images_formset = PetImageFormset(queryset=pet_post.pet_images, prefix='pet_images')
    ctx = {
        'pet_form': pet_form,
        'pet_images_formset': pet_images_formset
    }
    return render(request, template, ctx)


