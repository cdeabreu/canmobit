from django.db import models
from django.utils.translation import ugettext_lazy as _

from .behaviors import Descriptable, Timestampable, Localizable
from authentication.models import User
# Create your models here.


DONATION_STATUS = (
    ('donated', _('Donado')),
    ('available', _('Disponible'))
)

STATUS = (
    ('missing', _('Perdido')),
    ('finded', _('Encontrado'))
)

AGE = (
    ('puppy', _('Cachorro')),
    ('young', _('Joven')),
    ('adult', _('Adulto'))
)

SIZE = (
    ('small', _('Chico')),
    ('medium', _('Mediano')),
    ('large', _('Grande'))
)

SEX = (
    ('female', _('Hembra')),
    ('male', _('Macho'))
)

PET = (
    ('dog', _('Perro')),
    ('cat', _('Gato')),
    ('other', _('Otros')),
    ('capi', _('Carpincho'))
)


class Pet(Descriptable, Timestampable, Localizable, models.Model):
    created_by = models.ForeignKey(User, on_delete=models.CASCADE, related_name='publications')
    status = models.CharField(_('Estado'), max_length=80, choices=STATUS)
    type = models.CharField(_('Tipo de mascota'), max_length=80, choices=PET)
    age_estimation = models.CharField(_('Edad aproximada'), choices=AGE, max_length=20)
    sex = models.CharField(_('Sexo'), choices=SEX, max_length=80)
    size = models.CharField(_('Tamaño'), choices=SIZE, max_length=20)
    color = models.CharField(_('Color'), max_length=40)
    hair = models.CharField(_('Pelaje'), max_length=40)

    def __str__(self):
        return f'{self.type}, {self.status}, {self.location}'


class PetImage(models.Model):
    image = models.ImageField(_('Imagen'), upload_to='posts/images/pets/')
    post = models.ForeignKey(Pet, on_delete=models.CASCADE, related_name='pet_images')

    def __str__(self):
        return f'pet image of {self.post}'


class Donation(Descriptable, Timestampable, Localizable, models.Model):
    created_by = models.ForeignKey(User, on_delete=models.CASCADE, related_name='donation_publications')
    status = models.CharField(_('Estado'), max_length=20, choices=DONATION_STATUS)

    def __str__(self):
        return f'{self.status}, {self.location}'

class DonationImage(models.Model):
    image = models.ImageField(_('Imagen'), upload_to='posts/images/donations/')
    post = models.ForeignKey(Donation, on_delete=models.CASCADE, related_name='donation_images')

    def __str__(self):
        return f'donation image of {self.post}'
