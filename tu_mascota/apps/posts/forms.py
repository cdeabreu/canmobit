from django import forms

from .models import Pet, PetImage, Donation, DonationImage

class PetForm(forms.ModelForm):

    class Meta:
        model = Pet
        exclude = ('created_by',)

class DonationForm(forms.ModelForm):

    class Meta:
        model = Donation
        exclude = ('created_by',)


PetImageFormset = forms.modelformset_factory(PetImage, extra=4, exclude=('post',), can_delete=True)
DonationImageFormset = forms.modelformset_factory(DonationImage, exclude=('post',), can_delete=True)
