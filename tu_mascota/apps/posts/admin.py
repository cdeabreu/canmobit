from django.contrib import admin

from .models import (
    Pet, PetImage, Donation, DonationImage
)

# Register your models here.

class PetImageInline(admin.TabularInline):
    model = PetImage
    extra = 0


class PetAdmin(admin.ModelAdmin):
    list_display = ('type', 'age_estimation', 'size',  'status', 'state')
    list_filter = ('creation_date', 'type', 'age_estimation', 'status', 'state')
    inlines = (PetImageInline,)


class DonationImageInline(admin.TabularInline):
    model = DonationImage
    extra = 0


class DonationAdmin(admin.ModelAdmin):
    list_display = ('status', 'state')
    list_filter = ('creation_date', 'status')
    inlines = (DonationImageInline,)


admin.site.register(Pet, PetAdmin)
admin.site.register(Donation, DonationAdmin)
