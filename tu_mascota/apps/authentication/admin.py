from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin

from .models import User
from .forms import UserChangeForm, UserCreationForm
# Register your models here.


class CustomUserAdmin(BaseUserAdmin):
    model = User
    list_display = ('username', 'full_name', 'email', 'is_active')
    list_filter = ('is_active', 'is_staff', 'is_admin')


class UserAdmin(BaseUserAdmin):
    # The forms to add and change user instances
    form = UserChangeForm
    add_form = UserCreationForm

    # The fields to be used in displaying the User model.
    # These override the definitions on the base UserAdmin
    # that reference specific fields on auth.User.
    list_display = ('username', 'full_name', 'email', 'is_active')
    list_filter = ('is_active', 'is_staff', 'is_admin')
    # add_fieldsets is not a standard ModelAdmin attribute. UserAdmin
    # overrides get_fieldsets to use this attribute when creating a user.
    search_fields = ('email', 'username', 'address', 'full_name')
    ordering = ('full_name',)
    filter_horizontal = ()

admin.site.register(User, CustomUserAdmin)
