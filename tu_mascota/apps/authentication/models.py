import pytz
from django.utils import timezone as dj_timezone

from django.db import models
from django.contrib.auth.models import PermissionsMixin
from django.contrib.auth.models import (
    BaseUserManager, AbstractBaseUser
)
from django.utils.translation import ugettext_lazy as _

from .manager import UserManager
# Create your models here.


class User(PermissionsMixin, AbstractBaseUser):
    username = models.CharField(_("Usuario"), max_length=50, unique=True)
    email = models.EmailField(_("Email"), max_length=254, unique=True)

    full_name = models.CharField(_("Nombre completo"), max_length=50, null=True, blank=True)
    address = models.CharField(_("Dirección"), max_length=80)
    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['email', 'full_name', 'address']

    objects = UserManager()

    is_active = models.BooleanField(_("Is active"), default=False)
    is_staff = models.BooleanField(_("Is staff"), default=False)
    is_admin = models.BooleanField(_("Is admin"), default=False)
    is_superuser = models.BooleanField(_("Is superuser"), default=False)

    date_joined = models.DateTimeField(_("Date joined"), default=dj_timezone.now())

