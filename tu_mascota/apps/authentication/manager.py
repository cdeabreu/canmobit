
from django.utils import timezone as dj_timezone
from django.contrib.auth.models import (
    BaseUserManager
)


class UserManager(BaseUserManager):
    def create_user(self, username, email, full_name, address, password=None):
        """
        Creates and saves a User with the given email, date of
        birth and password.
        """
        if not email:
            raise ValueError('Users must have an email address')

        user = self.model(
            username=username,
            email=self.normalize_email(email),
            full_name=full_name,
            address=address
        )

        user.is_active = True
        user.set_password(password)
        user.date_joined = dj_timezone.now()
        user.save(using=self._db)
        return user

    def create_superuser(self, username, email, full_name, address, password=None):
        """
        Creates and saves a superuser with the given email, date of
        birth and password.
        """
        user = self.create_user(
            username=username,
            email=self.normalize_email(email),
            full_name=full_name,
            address=address
        )
        user.is_admin = True
        user.is_active = True
        user.is_staff = True
        user.is_superuser = True
        user.date_joined = dj_timezone.now()
        user.set_password(password)
        user.save(using=self._db)
        return user
